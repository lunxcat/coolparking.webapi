﻿namespace CoolParking.WebAPI.Model
{
    public struct TransactionInfoNonDate
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}

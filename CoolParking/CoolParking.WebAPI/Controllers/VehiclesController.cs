﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService parkingService;
        public VehiclesController(IParkingService service)
        {
            parkingService = service;
        }

        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicle()
        {
            return Ok(parkingService.GetVehicles());
        }

        // GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            // If id is invalid - Status Code: 400 Bad Request
            if (!ValidationCheck(id)) return BadRequest();

            ReadOnlyCollection<Vehicle>  vehicles = parkingService.GetVehicles();
            foreach (Vehicle vehicle in vehicles)
            {
                if(vehicle.Id == id)
                {
                    return Ok(vehicle);
                }
            }
            return new NotFoundResult();
        }

        // POST api/<VehiclesController>
        [HttpPost]
        public ActionResult Post([FromBody] Vehicle vehicle)
        {
            // If body is invalid - Status Code: 400 Bad Request
            if (!ValidationCheck(vehicle.Id)) return BadRequest();

            // If request is handled successfully - Status Code: 201 Created
            parkingService.AddVehicle(vehicle);
            return StatusCode(StatusCodes.Status201Created);
        }

        // DELETE api/<VehiclesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            // If id is invalid - Status Code: 400 Bad Request
            if (!ValidationCheck(id)) return BadRequest();

            // If vehicle not found -Status Code: 404 Not Found
            ReadOnlyCollection<Vehicle> vehicles = parkingService.GetVehicles();
            foreach (Vehicle vehicle in vehicles)
            {
                if (vehicle.Id == id)
                {
                    parkingService.RemoveVehicle(vehicle.Id);
                    return StatusCode(StatusCodes.Status204NoContent);
                }
            }
            return new NotFoundResult();
        }

        private bool ValidationCheck(string id)
        {
            if (id.Length != 10) {
                return false;
            };

            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}$";

            return Regex.IsMatch(id, pattern);
        }
    }
}

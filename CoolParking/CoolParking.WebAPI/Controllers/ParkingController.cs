﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/<ParkingController>/balanse
        [HttpGet("balanse")]
        public ActionResult<decimal> GetBalanse()
        {
            decimal balanse = parkingService.GetBalance();
            return Ok(balanse);
        }

        // GET: api/<ParkingController>/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            int capacity = parkingService.GetCapacity();
            return Ok(capacity);
        }

        // GET: api/<ParkingController>/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> FreePlaces()
        {
            int freePlaces = parkingService.GetFreePlaces();
            return Ok(freePlaces);
        }
    }
}

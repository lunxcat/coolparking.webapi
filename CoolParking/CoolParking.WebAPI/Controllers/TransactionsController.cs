﻿using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService parkingService;
        public TransactionsController(IParkingService service)
        {
            parkingService = service;
        }

        // GET: api/<TransactionsController>
        [HttpGet("last")]
        public ActionResult<TransactionInfo> GetLast()
        {
            return Ok(parkingService.GetLastParkingTransactions());
        }

        // GET: api/<TransactionsController>
        [HttpGet("all")]
        public ActionResult<TransactionInfo> GetAll()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch
            {
                return new NotFoundResult();
            }
        }

        // PUT api/<TransactionsController>
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> Put([FromBody] TransactionInfoNonDate transactionInfoNonDate)
        {
            foreach (Vehicle vehicle in parkingService.GetVehicles())
            {
                if (vehicle.Id == transactionInfoNonDate.Id)
                {
                    parkingService.TopUpVehicle(transactionInfoNonDate.Id, transactionInfoNonDate.Sum);
                    return Ok(vehicle);
                }
            }
            return new NotFoundResult();
        }
    }
}
﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

public class LogService : ILogService
{
    private string filePath;

    public LogService(string _logFilePath)
    {
        filePath = _logFilePath;
        ClearLog();
    }

    public string LogPath => filePath;

    public string Read()
    {
        using (StreamReader file = new StreamReader(filePath))
        {
            return file.ReadToEnd();
        }
    }

    public void Write(string logInfo)
    {
        using (StreamWriter writer = new StreamWriter(filePath, append: true))
        {
            writer.WriteLine(logInfo);
        }
    }

    private void ClearLog()
    {
        using (StreamWriter writer = new StreamWriter(filePath))
        {
            writer.Write("");
        }
    }
}
﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;

public class ParkingService : IParkingService
{
    static Parking parking { get; set; }
    List<TransactionInfo> transactionsInfo;
    ITimerService _withdrawTimer;
    ITimerService _logTimer;
    ILogService _logService;

    public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
    {
        parking = Parking.getInstance();

        parking.Balance = Settings.initialBalance;
        transactionsInfo = new List<TransactionInfo>();

        this._withdrawTimer = _withdrawTimer;
        this._withdrawTimer.Interval = 4000;
        this._withdrawTimer.Elapsed += AutoWriteOff;
        this._withdrawTimer.Start();
        this._logTimer = _logTimer;
        this._logTimer.Interval = 8000;
        this._logTimer.Elapsed += AutoWriteTransaction;
        this._logService = _logService;
    }

    public void AddVehicle(Vehicle vehicle)
    {
        if (parking.Vehicles.Count < Settings.parkingCapacity)
        {
            
            if (parking.FindVehicleById(vehicle.Id) == null)
            {
                parking.Vehicles.Add(vehicle);
            }
            else
            {
                throw new ArgumentException(String.Format("{0} such a machine exists", vehicle.Id),
                                     "Id");
            }
        }
        else
        {
            throw new System.InvalidOperationException();
        }
    }

    public void Dispose()
    {
        this.Dispose();
        _withdrawTimer.Dispose();
        _logTimer.Dispose();
        parking.Dispose();
    }

    public decimal GetBalance()
    {
        return parking.Balance;
    }

    public int GetCapacity()
    {
        return Settings.parkingCapacity;
    }

    public int GetFreePlaces()
    {
        return Settings.parkingCapacity - parking.Vehicles.Count;
    }

    public TransactionInfo[] GetLastParkingTransactions()
    {
        return new List<TransactionInfo>(transactionsInfo).ToArray();
    }

    public ReadOnlyCollection<Vehicle> GetVehicles()
    {
        return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
    }

    public string ReadFromLog()
    {
        return _logService.Read();
    }

    // забирати Тр. засоби з Паркінгу;
    public void RemoveVehicle(string vehicleId)
    {
        int index = parking.Vehicles.FindIndex(v => v.Id == vehicleId);
        if (parking.Vehicles[index].Balance >= 0)
        {
            parking.Vehicles.RemoveAt(index);
        }
        else
        {
            throw new System.InvalidOperationException();
        }
    }

    public void TopUpVehicle(string vehicleId, decimal sum)
    {
        foreach (Vehicle vehicle in parking.Vehicles)
        {
            if (vehicle.Id == vehicleId)
            {
                vehicle.Balance += sum;
                return;
            }
        }

        throw new ArgumentException(String.Format("{0} no such was found", vehicleId),
                                      "vehicleId");
    }

    private void AutoWriteOff(object source, ElapsedEventArgs e)
    {
        foreach (Vehicle vehicle in parking.Vehicles)
        {
            //Obrahunky
            decimal tarife = 0;
            switch (vehicle.VehicleType)
            {
                case VehicleType.Motorcycle:
                    tarife = Settings.motorcycleTarife;
                    break;
                case VehicleType.PassengerCar:
                    tarife = Settings.carTarife;
                    break;
                case VehicleType.Bus:
                    tarife = Settings.busTarife;
                    break;
                case VehicleType.Truck:
                    tarife = Settings.truckTarife;
                    break;
            }

            decimal sum = 0;
            // Perewirjajemo chy dostatnio groshej dla spysana
            if (vehicle.Balance >= tarife)
            {
                sum = tarife;
            }
            else
            {
                if (vehicle.Balance > 0)
                {
                    sum = vehicle.Balance + ((tarife - vehicle.Balance) * Settings.fineRate);
                }
                else
                {
                    sum = tarife * Settings.fineRate;
                }
            }

            vehicle.Balance -= sum;
            parking.Balance += sum;

            //Zapys Tranzakci
            TransactionInfo ti = new TransactionInfo(vehicle.Id, DateTime.Now, sum);
            transactionsInfo.Add(ti);
        }
    }

    private void AutoWriteTransaction(object source, ElapsedEventArgs e)
    {
        string result = "";
        foreach (TransactionInfo transactionInfo in transactionsInfo.ToArray())
        {
            result += transactionInfo.Id + " | " + transactionInfo.transactionTime + " | " + transactionInfo.Sum + "\n";
        }
        _logService.Write(result);
        transactionsInfo.Clear();
    }
}
﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Text.Json.Serialization;
    public class TransactionInfo
    {
        public string Id { get; set; }
        public DateTime transactionTime { get; }
        public decimal Sum { get; set; }

        public TransactionInfo(string Id, DateTime transactionTime, decimal Sum)
        {
            this.Id = Id;
            this.transactionTime = transactionTime;
            this.Sum = Sum;
        }
    }
﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using CoolParking.BL.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;

/*    public static class Parking
    {
        public static decimal Balance { get; set; }
        public static List<Vehicle> Vehicles = new List<Vehicle>();

        public static Vehicle FindVehicleById(string id)
        {
            Vehicle vehicle = Vehicles.Find(v => v.Id == id);
            return vehicle;
        }
    }*/

public class Parking
{
    private static Parking instance;
    public decimal Balance { get; set; }
    public List<Vehicle> Vehicles { get; private set; }
    private Parking()
    {
        Vehicles = new List<Vehicle>();
    }

    public static Parking getInstance()
    {
        if (instance == null)
            instance = new Parking();
        return instance;
    }

    public Vehicle FindVehicleById(string id)
    {
        Vehicle vehicle = Vehicles.Find(v => v.Id == id);
        return vehicle;
    }

    public void Dispose()
    {
        Balance = 0;
        Vehicles = new List<Vehicle>();
    }
}
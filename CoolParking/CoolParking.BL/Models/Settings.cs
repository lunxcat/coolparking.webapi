﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
    public static class Settings
    {
        public static int initialBalance = 0;
        public static int parkingCapacity = 10;
        public static int paymentPeriod = 5;
        public static int writeLogPeriod = 60;
        public static decimal carTarife = 2;
        public static decimal truckTarife = 5;
        public static decimal busTarife = 3.5M;
        public static decimal motorcycleTarife = 1;
        public static decimal fineRate = 2.5M;
    }
﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.

using System;
using System.Linq;

    public class Vehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }

        //  The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            this.Id = Id;
            this.VehicleType = VehicleType;
            this.Balance = Balance;
        }

        //  Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
        static string GenerateRandomRegistrationPlateNumber()
        {
            string registrationPlateNumber = RandomString() + "-";
            registrationPlateNumber += RandomNumber().ToString() + "-";
            registrationPlateNumber += RandomString();
            return registrationPlateNumber;
        }

        private static Random random = new Random();

        private static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 2)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static int RandomNumber()
        {
            return random.Next(1000, 9999);
        }
    }
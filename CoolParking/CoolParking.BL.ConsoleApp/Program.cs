﻿using System;

namespace CoolParking.BL.ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ParkingService _parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(@"C:\Users\Niko\Desktop\task\Transactions.log"));
            Menu(_parkingService);

            Console.ReadLine();
        }

        private static void Menu(ParkingService _parkingService)
        {
            MenuTask menuTask = new MenuTask(_parkingService);
            Console.WriteLine("Menu");
            Console.WriteLine("1. Display Current Balance\n2. Display Amount Of Money Earned\n3. Display Free Place\n4. Display Current All Transaction\n5. Display History Transaction\n6. Display Vehicle In The Parking\n7. PutIn The Parking\n 8. Remove Vehicle From Parking\n 9. Replenish Vehicle Balance");
            Console.WriteLine("Select an action");

            int menuItemNumber = 0;
            while (true)
            {
                string vehicleNumberString = Console.ReadLine();
                try
                {
                    menuItemNumber = Int32.Parse(vehicleNumberString);
                }
                catch
                {
                    Console.WriteLine("Error. Please write number! Please try again");
                }

                if (menuItemNumber > 0 && menuItemNumber <= 10)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Error. Enter a number from 1 to 10");
                }
            }

            switch (menuItemNumber)
            {
                case 1:
                    menuTask.DisplayCurrentBalance();
                    break;
                case 2:
                    menuTask.DisplayAmountOfMoneyEarned();
                    break;
                case 3:
                    menuTask.DisplayFreePlace();
                    break;
                case 4:
                    menuTask.DisplayCurrentAllTransaction();
                    break;
                case 5:
                    menuTask.DisplayHistoryTransaction();
                    break;
                case 6:
                    menuTask.DisplayVehicleInTheParking();
                    break;
                case 7:
                    menuTask.PutInTheParking();
                    break;
                case 8:
                    menuTask.RemoveVehicleFromParking();
                    break;
                default:
                    menuTask.ReplenishVehicleBalance();
                    break;
            }
            Menu(_parkingService);
        }
    }
}
